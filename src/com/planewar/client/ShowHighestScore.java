package com.planewar.client;
/**
 * 
 * 飞机大战最高分控制界面类
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 *
 */
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import com.planewar.constant.Constant;
import com.planewar.core.Images;

public class ShowHighestScore extends Frame{
	private Button bt;
	private PlaneWarControler pwc;
	public ShowHighestScore(PlaneWarControler pwc) {
		this.pwc=pwc;
		madeFrame();
	}

	public void madeFrame() {
		this.setTitle(Constant.GAME_TITTLE);
		this.setSize(Constant.GAME_WIDTH,Constant.GAME_HEIGHT);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		this.setLayout(new FlowLayout(FlowLayout.CENTER, 200, 400));
		bt=new Button("返回");
		bt.setPreferredSize(new Dimension(150,60));
		bt.setForeground(Color.DARK_GRAY);
		bt.setBackground(Color.cyan);
		bt.setFont(new Font("幼圆", Font.CENTER_BASELINE, 30));
		this.add(bt);
		myEvent(this);
		this.setVisible(false);
		new ShowThread().start();
	}

	private void myEvent(ShowHighestScore shs) {
		bt.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shs.setVisible(false);
				pwc.setVisible(true);
			}
		});
	}
	@Override
	public void paint(Graphics g) {
		g.drawImage(Images.imgs.get("stage_05"), 0, 0, null);
		int x=Constant.GAME_WIDTH/2-PlaneWarControler.strh.length()*30;
		Color c=g.getColor();
		g.setColor(Color.pink);
		Font f=g.getFont();
		g.setFont(new Font("幼圆", Font.BOLD,100));
		g.drawString(PlaneWarControler.strh, x, Constant.GAME_HEIGHT/2);
		g.setColor(c);
		g.setFont(f);
	}
	class ShowThread extends Thread{
		@Override
		public void run() {
			while(true) {
				repaint();
			}
		}
	}
}
