package com.planewar.client;
/**
 * 
 * 飞机大战客户端类，用户交互界面与程序控制
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 *
 */
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import com.planewar.constant.Constant;
import com.planewar.core.BackgroundMusic;
import com.planewar.core.Images;
import com.planewar.core.MyFrame;


public class PlaneWarControler extends Frame{
	private ShowHighestScore shs=new ShowHighestScore(this);
	
	public static BackgroundMusic bgm;
	
	//最高分
	public static int highestScore;
	public static String strh;
	//开始按钮
	private Button sbt;
	//最高分查询按钮
	private Button hbt;
	//退出按钮
	private Button ebt;
	private Button clear;
	public PlaneWarControler() {
		madeFrame();
	}

	public void madeFrame() {
		bgm=new BackgroundMusic("src/com/planewar/sounds/controlMenu.wav");
		bgm.start();
		this.setSize(Constant.GAME_WIDTH,Constant.GAME_HEIGHT);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setResizable(false);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		this.setTitle(Constant.GAME_TITTLE);
		//按钮距离上边框位置  按钮间距
		this.setLayout(new FlowLayout(FlowLayout.CENTER, 200, 100));
		sbt=new Button("开始游戏");
		hbt=new Button("最高分");
		clear=new Button("清空数据");
		ebt=new Button("退出游戏");
		sbt.setPreferredSize(new Dimension(150, 60));//按钮大小
		hbt.setPreferredSize(new Dimension(150, 60));//按钮大小
		clear.setPreferredSize(new Dimension(150, 60));
		ebt.setPreferredSize(new Dimension(150, 60));
		sbt.setForeground(Color.DARK_GRAY);//按钮字体颜色
		hbt.setForeground(Color.DARK_GRAY);
		clear.setForeground(Color.DARK_GRAY);
		ebt.setForeground(Color.DARK_GRAY);
		sbt.setBackground(Color.CYAN);//按钮背景颜色
		hbt.setBackground(Color.CYAN);
		clear.setBackground(Color.CYAN);
		ebt.setBackground(Color.CYAN);
		sbt.setFont(new Font("幼圆", Font.CENTER_BASELINE, 30));//设置按钮文字的字体，样式，大小
		hbt.setFont(new Font("幼圆", Font.CENTER_BASELINE, 30));
		clear.setFont(new Font("幼圆", Font.CENTER_BASELINE, 30));
		ebt.setFont(new Font("幼圆", Font.CENTER_BASELINE, 30));
		this.add(sbt);
		this.add(hbt);
		this.add(clear);
		this.add(ebt);
		myEvent(this);
	}
/**
 * 按钮组件和鼠标监听方法
 * */
	private void myEvent(PlaneWarControler pwc) {
		sbt.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MyFrame.start=true;
				new PlaneWarSystem(pwc).launchFrame();
				pwc.setVisible(false);
				bgm.isContinue=false;
			}
		});
		hbt.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				getHighestScore();
				shs.setVisible(true);
				pwc.setVisible(false);
			}
		});
		clear.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				highestScore=0;
				setHighestScore();
			}
		});
		ebt.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
	}
	/**
	 * 将历史最高分存入文件的类
	 * */
	public static void setHighestScore() {
		FileOutputStream fos=null;
		try {
			fos=new FileOutputStream("src/DataSet/highestScore.txt");
			String str=((Integer)highestScore).toString();
			for(int i=0;i<str.length();i++) {
				fos.write((int)(str.charAt(i)));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	/**
	 * 从文件中读取历史最高分的方法
	 * */
	public static void getHighestScore() {
		FileInputStream fis=null;
		try {
			fis=new FileInputStream("src/DataSet/highestScore.txt");
			int index=0;
			int i=0;
			char[] chs=new char[0];
			while((index=fis.read())!=-1) {
				chs=Arrays.copyOf(chs, chs.length+1);
				chs[i]=(char)index;
				i++;
			}
			strh=new String(chs);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void paint(Graphics g) {
		g.drawImage(Images.imgs.get("stage_05"), 0, 0, null);
	}
	//程序主方法入口
	public static void main(String[] args) {
		new PlaneWarControler();
	}
}
