package com.planewar.client;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import com.planewar.constant.Constant;
import com.planewar.core.AePlayWave;
import com.planewar.core.BackgroundMusic;
import com.planewar.core.MyFrame;
import com.planewar.entity.Background;
import com.planewar.entity.Boss;
import com.planewar.entity.Bullet;
import com.planewar.entity.EnemyPlane;
import com.planewar.entity.Explode;
import com.planewar.entity.Item;
import com.planewar.entity.MainPlane;
import com.planewar.entity.Plane;

/**
 * 
 * 飞机大战主类，各对象和各类的主控制类
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 *
 */
public class PlaneWarSystem extends MyFrame{
	
	public static BackgroundMusic bgm;
	
	public static PlaneWarControler pwc;
	
	public PlaneWarSystem pws=this;

	//创建我方飞机
	public MainPlane myPlane=new MainPlane(200, 600,this);
	
	//创建敌人飞机的容器
	public ArrayList<Plane> enemyPlanes=new ArrayList<>();
	/**
	 * 创建子弹的容器
	 * */
	public ArrayList<Bullet> bullets=new ArrayList<>();
	
	/**
	 * 创建爆炸的容器
	 * */
	public ArrayList<Explode> explodes=new ArrayList<>();
	
	/**
	 * 创建障碍物
	 * */
//	public Obstruction ob=new Obstruction(100, 100, 500, 30, this);
	/**
	 * 创建道具
	 * */
	public ArrayList<Item> items=new ArrayList<>();
	
	//创建boss
	public ArrayList<Plane> bosses=new ArrayList<>();
	
	//创建背景
	public Background bg=new Background();
	
	public PlaneWarSystem(PlaneWarControler pwc) {
		PlaneWarSystem.pwc=pwc;
	}
	
	@Override
	public void launchFrame() {
		super.launchFrame();
		bgm=new BackgroundMusic("src/com/planewar/sounds/stage_01.wav");
		bgm.start();
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				pws.setVisible(false);
				pwc.setVisible(true);
				PlaneWarControler.bgm.isContinue=true;
				if(PlaneWarSystem.bgm.isAlive()) {
					PlaneWarSystem.bgm.isPlay=false;
				}
				Bullet.bgm.isPlay=false;
			}
		});
		this.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				myPlane.keyPressed(e);
			}
			@Override
			public void keyReleased(KeyEvent e) {
				myPlane.keyReleased(e);
			}
		});
		this.setLayout(new FlowLayout(FlowLayout.CENTER, 200, 400));
		bosses.add(new Boss(this));
	}
	/**
	 * 项目中所有实体类都在此方法中画出
	 * 
	 * */
	@Override
	public void paint(Graphics g) {
		bg.draw(g);

		//我方飞机的信息
		Color c=g.getColor();
		Font f=g.getFont();
		g.setColor(Color.yellow);
		g.setFont(new Font("幼圆", Font.BOLD, 12));
		g.drawString("血量："+myPlane.getBlood(), 10, 40);
		g.drawString("等级："+myPlane.getLevel(), 10, 60);
		g.drawString("生命："+myPlane.getLife(), 10, 80);
		g.drawString("分数："+Bullet.score, 10, 100);
		g.drawString("关卡："+Bullet.stage, 10, 120);
		g.setColor(c);
		g.setFont(f);
		
		//分高升级
		if(Bullet.hideScore>40000) {
			if(myPlane.getLevel()<5) {
				myPlane.setLevel(myPlane.getLevel()+1);
				new AePlayWave("src/com/planewar/sounds/eatItem.wav").start();
			}
			Bullet.hideScore=0;
		}
		
		for(int i=0;i<bullets.size();i++) {
			Bullet bullet=bullets.get(i);
			//画出子弹
			bullet.draw(g);
			bullet.hitPlane(enemyPlanes);
			bullet.hitPlane(myPlane);
			bullet.hitBoss(bosses);
//			bullet.hitObstruction(ob);
		}
		//生成敌人飞机
		if(Math.random()<=0.001*Bullet.stage*bg.getSpeed()&&bosses.size()>0&&bosses.get(0).y<0&&enemyPlanes.size()<=(bg.getSpeed()+Bullet.stage)) {
			for(int i=0;i<Math.random()*bg.getSpeed();i++) {
				EnemyPlane enemyPlane=new EnemyPlane((int)(Math.random()*(Constant.GAME_WIDTH-66)), -30, this,1);
				this.enemyPlanes.add(enemyPlane);
			}
		}
		if(Math.random()<=0.0009*Bullet.stage*bg.getSpeed()/2&&bosses.size()>0&&bosses.get(0).y<0&&enemyPlanes.size()<=(bg.getSpeed()+Bullet.stage)/2) {
			for(int i=0;i<Math.random()*bg.getSpeed();i++) {
				EnemyPlane enemyPlane=new EnemyPlane((int)(Math.random()*(Constant.GAME_WIDTH-66)), -30, this,2);
				this.enemyPlanes.add(enemyPlane);
			}
		}
		if(Bullet.stage>1&&Math.random()<=0.0008*Bullet.stage*bg.getSpeed()/3&&bosses.size()>0&&bosses.get(0).y<0&&enemyPlanes.size()<=(bg.getSpeed()+Bullet.stage)/3) {
			for(int i=0;i<Math.random()*bg.getSpeed();i++) {
				EnemyPlane enemyPlane=new EnemyPlane((int)(Math.random()*(Constant.GAME_WIDTH-66)), -30, this,3);
				this.enemyPlanes.add(enemyPlane);
			}
		}
		if(Bullet.stage>2&&Math.random()<=0.0007*Bullet.stage*bg.getSpeed()/4&&bosses.size()>0&&bosses.get(0).y<0&&enemyPlanes.size()<=(bg.getSpeed()+Bullet.stage)/4) {
			for(int i=0;i<Math.random()*bg.getSpeed();i++) {
				EnemyPlane enemyPlane=new EnemyPlane((int)(Math.random()*(Constant.GAME_WIDTH-66)), -30, this,4);
				this.enemyPlanes.add(enemyPlane);
			}
		}
		if(Bullet.stage>3&&Math.random()<=0.0006*Bullet.stage*bg.getSpeed()/5&&bosses.size()>0&&bosses.get(0).y<0&&enemyPlanes.size()<=(bg.getSpeed()+Bullet.stage)/5) {
			for(int i=0;i<Math.random()*bg.getSpeed();i++) {
				EnemyPlane enemyPlane=new EnemyPlane((int)(Math.random()*(Constant.GAME_WIDTH-66)), -30, this,5);
				this.enemyPlanes.add(enemyPlane);
			}
		}
		if(Bullet.stage>4&&Math.random()<=0.0005*Bullet.stage*bg.getSpeed()/6&&bosses.size()>0&&bosses.get(0).y<0&&enemyPlanes.size()<=(bg.getSpeed()+Bullet.stage)) {
			for(int i=0;i<Math.random()*bg.getSpeed();i++) {
				EnemyPlane enemyPlane=new EnemyPlane((int)(Math.random()*(Constant.GAME_WIDTH-66)), -30, this,6);
				this.enemyPlanes.add(enemyPlane);
			}
		}
		for(int i=0;i<enemyPlanes.size();i++) {
			//获取到容器中的每一架飞机
			Plane plane=enemyPlanes.get(i);
			//画出每一架飞机
			plane.draw(g);
		}
		//一堆子弹画出来
		
		//画出一堆爆炸
		for(int i=0;i<explodes.size();i++) {
			Explode e=explodes.get(i);
//			if(e.isLive()) {
				e.draw(g);
//			}
			
		}
		
		//画出我方飞机
		if(myPlane.isLive()) {
			myPlane.draw(g);
			myPlane.hitPlane(enemyPlanes);
			myPlane.hitPlane(bosses);
		}
		
//		if(myPlanes.get(0).isLive()) {
//			myPlanes.get(0).draw(g);
//		}
		/**
		 * 画出障碍物
		 * */
//		ob.draw(g);
		
		//画出道具
		
		for(int i=0;i<items.size();i++) {
			Item item=items.get(i);
			item.draw(g);
		}
		myPlane.eatItem(items);
		
		//画出boss
		for(int i=0;i<bosses.size();i++) {
			Boss boss=(Boss)(bosses.get(i));
			boss.draw(g);
		}
		//通关画面
		if(Bullet.stage==6&&bosses.size()<=0&&enemyPlanes.size()<=0) {
			g.setColor(Color.pink);
			g.setFont(new Font("幼圆", Font.BOLD,100));
			g.drawString("恭喜通关！", 30, Constant.GAME_HEIGHT/2);
			g.setColor(c);
			g.setFont(f);
			if(Explode.isHighestScore()) {
				PlaneWarControler.highestScore=Bullet.score;
				PlaneWarControler.setHighestScore();
			}
			Bullet.score=0;
			Bullet.hideScore=0;
			if(explodes.size()<=0) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.setVisible(false);
				MyFrame.start=false;
				pwc.setVisible(true);
				Bullet.bgm.isPlay=false;
				PlaneWarControler.bgm.isContinue=true;
			}
		}
		//失败画面
		if(myPlane.getLife()<=0) {
			g.setColor(Color.red);
			g.setFont(new Font("幼圆", Font.ITALIC,100));
			g.drawString("失败！", 150, Constant.GAME_HEIGHT/2);
			g.setColor(c);
			g.setFont(f);
			if(Explode.isHighestScore()) {
				PlaneWarControler.highestScore=Bullet.score;
				PlaneWarControler.setHighestScore();
			}
			Bullet.score=0;
			Bullet.hideScore=0;
			if(explodes.size()<=0) {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Bullet.stage=1;
				this.setVisible(false);
				MyFrame.start=false;
				pwc.setVisible(true);
				PlaneWarControler.bgm.isContinue=true;
				if(PlaneWarSystem.bgm.isAlive()) {
					PlaneWarSystem.bgm.isPlay=false;
				}
				Bullet.bgm.isPlay=false;
			}
		}
		//暂停画面
		if(MainPlane.drawPaused) {
			g.setColor(Color.pink);
			g.setFont(new Font("幼圆", Font.BOLD,100));
			g.drawString("暂停中", 100, Constant.GAME_HEIGHT/2);
			g.setColor(c);
			g.setFont(f);
			MyFrame.isContinue=false;
		}
		//重生
		if(myPlane.getLife()>0&&myPlane.getBlood()<=0) {
			myPlane.setLife(myPlane.getLife()-1);
			myPlane.setBlood(100);
		}
		if(myPlane.getLife()>0&&!myPlane.isLive()&&explodes.size()<=0) {
			myPlane.setLive(true);
			myPlane.setLevel(1);
			myPlane.x=200;
			myPlane.y=600;
		}
	}
	
}
