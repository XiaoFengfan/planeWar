package com.planewar.constant;

import com.planewar.util.GameUtil;

/**
 * 项目中的常量
 * 
 * @author 丁奇
 * @since 2017-09-04
 * @version 1.0
 * 
 * */
public class Constant {
	public static final int GAME_WIDTH=Integer.parseInt(GameUtil.prop.getProperty("GAME_WIDTH"));
	public static final int GAME_HEIGHT=Integer.parseInt(GameUtil.prop.getProperty("GAME_HEIGHT"));;
	public static final String GAME_TITTLE="飞机大战";
	public static final String IMG_PRE="com/planewar/img/";
	public static final String IMG_AFTER=".png";
	public static final int ENERMY_PLANE_NUM=5;
	public static final String[] bgms= {
			"src/com/planewar/sounds/stage_02.wav",
			"src/com/planewar/sounds/stage_03.wav",
			"src/com/planewar/sounds/stage_04.wav",
			"src/com/planewar/sounds/stage_05.wav",
			"src/com/planewar/sounds/stage_06.wav"
	};
}
