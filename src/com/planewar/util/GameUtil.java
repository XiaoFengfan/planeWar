package com.planewar.util;

/**
 * 
 * 飞机大战主运行类，运行飞机大战项目
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 *
 */
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import javax.imageio.ImageIO;

/**
 * 通过配置文件加载属性值
 * 获取图片的方法
 * 获取音频的方法
 * @param imgpath 图片的路径
 * @return 根据图片的路径返回该路径下的图片对象
 * @param audiopath 音频的路径
 * @return 根据音频的路径返回该路径下的AudioClip对象
 * */
public class GameUtil {
	
	public static Properties prop=new Properties();
	static {
		try {
			prop.load(GameUtil.class.getClassLoader().getResourceAsStream("planewar.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Image getImage(String imgpath) {
		URL u=GameUtil.class.getClassLoader().getResource(imgpath);
		BufferedImage img=null;
		try {
			img=ImageIO.read(u);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}
}
