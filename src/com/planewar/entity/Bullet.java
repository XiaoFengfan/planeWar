package com.planewar.entity;

import java.awt.Graphics;
import java.awt.Image;
import java.util.List;
import java.util.Random;

import com.planewar.client.PlaneWarSystem;
import com.planewar.constant.Constant;
import com.planewar.core.AePlayWave;
import com.planewar.core.BackgroundMusic;
import com.planewar.core.Direction;
import com.planewar.core.Images;
import com.planewar.core.PlaneWarObject;

/**
 * 
 * 子弹类
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 * */
public class Bullet extends PlaneWarObject{
	
	public Direction dir;
	
	private int speed;
	
	/**
	 * 子弹生死的变量
	 * */
	private boolean live;
	
	PlaneWarSystem pws;
	
	private boolean good;//是否是自己飞机发出来的
	
	private int power;//威力
	
	static Random r=new Random();
	
	public static int score=0;
	
	public static int hideScore=0;
	
	//关卡数
	public static int stage=1;
	private int category;
	
	private static Image[] imgs= {
			Images.imgs.get("bullet_01_up"),
			Images.imgs.get("enemyBullet_02"),
			Images.imgs.get("bullet_03"),
			Images.imgs.get("bullet_04"),
			Images.imgs.get("bullet_05"),
			Images.imgs.get("bullet_06")
	};
	
	public static BackgroundMusic bgm=null;
	
	public Bullet() {
		
	}
	
	public Bullet(int x,int y,Direction dir,PlaneWarSystem pws,boolean good,int category) {
		this.x=x;
		this.y=y;
		this.dir=dir;
		this.live=true;
		this.pws=pws;
		this.good=good;
		this.setCategory(category);
		if(good) this.speed=20;
		else this.speed=10+category*4;

		switch(category) {
		case 0:
			this.img=imgs[0];
			this.setPower(10);
			break;
		case 1:
			this.img=imgs[1];
			this.setPower(10);
			break;
		case 2:
			this.img=imgs[2];
			this.setPower(15);
			break;
		case 3:
			this.img=imgs[3];
			this.setPower(20);
			break;
		case 4:
			this.img=imgs[4];
			this.setPower(25);
			break;
		case 5:
			this.img=imgs[5];
			this.setPower(30);
			break;
		}
		
		
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	@Override
	public void draw(Graphics g) {
		if(!live) {
			this.pws.bullets.remove(this);
		}
		g.drawImage(img, x, y, null);
		move();
	}

	@Override
	public void move() {
		//确定方向后如何移动
		switch (dir) {
		case LEFT:
			x-=speed;
			break;
		case LEFT_UP:
			x-=speed;
			y-=speed;
			break;
		case UP:
			y-=speed;
			break;
		case RIGHT_UP:
			x+=speed;
			y-=speed;
			break;
		case RIGHT:
			x+=speed;
			break;
		case RIGHT_DOWN:
			x+=speed;
			y+=speed;
			break;
		case DOWN:
			y+=speed;
			break;
		case LEFT_DOWN:
			x-=speed;
			y+=speed;
			break;
		default:
			dir=Direction.STOP;
			break;
		}
		if(outOfBounds()) {
			this.live=false;
		}
	}
	/**
	 * 判断子弹出界问题
	 * 
	 * */
	private boolean outOfBounds() {
		//出界的条件判断
		if(x<0||x>Constant.GAME_WIDTH-this.img.getWidth(null)||
				y<30||y>Constant.GAME_HEIGHT-this.img.getHeight(null)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 子弹击打一个飞机
	 * */
	public boolean hitPlane(Plane plane) {
		//判断子弹打到飞机的逻辑
		if(live&&plane.isLive()&&this.getRect().intersects(plane.getRect())&&this.good!=plane.isGood()) {
			//子弹消失
			this.live=false;
			if(plane.isGood()) {
				new AePlayWave("src/com/planewar/sounds/beHited.wav").start();
				plane.setBlood(plane.getBlood()-this.power);
				if(plane.getBlood()<=0){
					Explode e=new Explode(plane.x, plane.y,pws);
					this.pws.explodes.add(e);
					new AePlayWave("src/com/planewar/sounds/explodeSelf.wav").start();
					//飞机消失
					plane.setLive(false);
				}
				return true;
			}else {
				plane.setBlood(plane.getBlood()-this.power);
				if(plane.getBlood()<=0) {
					Explode e=new Explode(plane.x, plane.y,pws);
					this.pws.explodes.add(e);
					new AePlayWave("src/com/planewar/sounds/explodePlane.wav").start();
					//飞机消失
					plane.setLive(false);
					//加分
					score+=100*plane.getLevel();
					hideScore+=100*plane.getLevel();
					//生成一个道具
					if(r.nextInt(1000)>(980-Bullet.stage*2)) {
						Item item=new Item(pws,plane.x,plane.y,0);
						pws.items.add(item);
					}
					if(r.nextInt(1000)>(970-Bullet.stage*4)) {
						Item item=new Item(pws,plane.x,plane.y,1);
						pws.items.add(item);
					}
					if(r.nextInt(1000)>(980-Bullet.stage*3)) {
						Item item=new Item(pws,plane.x,plane.y,2);
						pws.items.add(item);
					}
					if(r.nextInt(1000)>(990-Bullet.stage*2)) {
						Item item=new Item(pws,plane.x,plane.y,3);
						pws.items.add(item);
					}
					if(r.nextInt(1000)>(995-Bullet.stage)) {
						Item item=new Item(pws,plane.x,plane.y,4);
						pws.items.add(item);
					}
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 子弹击打一堆飞机
	 * */
	public boolean hitPlane(List<Plane> planes) {
		for(int i=0;i<planes.size();i++) {
			Plane plane=planes.get(i);
			if(this.hitPlane(plane)) return true;
		}
		return false;
	}

	public boolean isGood() {
		return good;
	}

	public void setGood(boolean good) {
		this.good = good;
	}
	
	/**
	 * 子弹击打障碍物
	 * */
	public boolean hitObstruction(Obstruction ob) {
		if(this.getRect().intersects(ob.getRect())) {
			this.live=false;
			return true;
		}
		return false;
	}
	
	/**
	 * 子弹击打障碍物们
	 * */
	public boolean hitObstruction(List<Obstruction> obs) {
		for(int i=0;i<obs.size();i++) {
			Obstruction ob=obs.get(i);
			if(hitObstruction(ob)) {
				
				return true;
			}
		}
		
		return false;
	}
	
	public boolean hitBoss(Plane boss) {
		if(live&&boss.isLive()&&this.getRect().intersects(boss.getRect())&&this.good!=boss.isGood()) {
//			System.out.println("打到了。。。");
			//子弹消失
			this.live=false;
			boss.setBlood(boss.getBlood()-this.power);
			if(boss.getBlood()<=0){
				Explode e=new Explode(boss.x, boss.y,pws);
				this.pws.explodes.add(e);
				new AePlayWave("src/com/planewar/sounds/explodeBoss.wav").start();
				//飞机消失
				boss.setLive(false);
				score+=5000+2500*Bullet.stage;
				hideScore+=5000+2500*Bullet.stage;
				Item item0=new Item(pws,boss.x,boss.y,0);
				Item item2=new Item(pws,boss.x,boss.y,2);
				pws.items.add(item0);
				pws.items.add(item2);
				if(stage<6) {
					if(stage==1) {
						PlaneWarSystem.bgm.isPlay=false;
						bgm=new BackgroundMusic(Constant.bgms[0]);
						bgm.start();
					}else {
						bgm.isPlay=false;
						bgm=new BackgroundMusic(Constant.bgms[stage-1]);
						bgm.start();
					}
					pws.bg.setSpeed(3);
					pws.bg.img=Background.imgs[stage];
					Plane newBoss=new Boss(pws);
					newBoss.setBlood(3000+stage*1000);
					newBoss.img=Boss.imgs[stage];
					pws.bosses.add(newBoss);
					stage++;
				}
			}
			return true;
		}
		return false;
	}
	public boolean hitBoss(List<Plane> bosses) {
		for(int i=0;i<bosses.size();i++) {
			Boss boss=(Boss)(bosses.get(i));
			this.hitBoss(boss);
		}
		return false;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

}
