package com.planewar.entity;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * 
 * 我方飞机类
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 * */
import com.planewar.client.PlaneWarSystem;
import com.planewar.constant.Constant;
import com.planewar.core.AePlayWave;
import com.planewar.core.Direction;
import com.planewar.core.Images;
import com.planewar.core.MyFrame;

public class MainPlane extends Plane{
	
	private int life;//生命
	
	public static boolean drawPaused;//画出暂停画面
	
	public MainPlane() {
		
	}
	
	public MainPlane(int x,int y,PlaneWarSystem pws) {
		super(x,y,pws);
		this.level=1;
		this.img=Images.imgs.get("mainPlane_01");
		this.dir=Direction.STOP;
		this.speed=10;
		this.good=true;
		this.life=3;
		this.blood=100;
	}
	
	@Override
	public void draw(Graphics g) {
		bb.draw(g);
		super.draw(g);
		move();
	}
	
	@Override
	public void move() {
		super.move();
		outOfBounds();
	}
	
	/**
	 * 确定方向的方法
	 * */
	private void confirmDirection() {
		if(left && !up && !right && !down) {
			dir=Direction.LEFT;
		}else if(left && up && !right && !down) {
			dir=Direction.LEFT_UP;
		}else if(left && !up && !right && down) {
			dir=Direction.LEFT_DOWN;
		}else if(!left && up && !right && !down) {
			dir=Direction.UP;
		}else if(!left && !up && !right && down) {
			dir=Direction.DOWN;
		}else if(!left && !up && right && !down) {
			dir=Direction.RIGHT;
		}else if(!left && up && right && !down) {
			dir=Direction.RIGHT_UP;
		}else if(!left && !up && right && down) {
			dir=Direction.RIGHT_DOWN;
		}else {
			dir=Direction.STOP;
		}
	}
	/**
	 * 主键飞机
	 * @param 键盘事件
	 * */
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
		case KeyEvent.VK_A:left=true;break;
		case KeyEvent.VK_D:right=true;break;
		case KeyEvent.VK_W:up=true;break;
		case KeyEvent.VK_S:down=true;break;
		}
		//每一次按键之后确定方向
		confirmDirection();
	}
	
	public void keyReleased(KeyEvent e) {
		switch(e.getKeyCode()) {
		case KeyEvent.VK_A:left=false;break;
		case KeyEvent.VK_D:right=false;break;
		case KeyEvent.VK_W:up=false;break;
		case KeyEvent.VK_S:down=false;break;
		case KeyEvent.VK_J:
			/**
			 * 按下J键发射一枚子弹
			 * */
			if(good&&live&&MyFrame.isContinue) {
				fire();
				new AePlayWave("src/com/planewar/sounds/bulletSound.wav").start();
			}
			break;
		case KeyEvent.VK_K:
			/**
			 * 控制暂停和继续
			 * */
			if(!MainPlane.drawPaused) {
				MainPlane.drawPaused=true;
			}
			else {
				MainPlane.drawPaused=false;
				MyFrame.isContinue=true;
			}
		default:break;
		}
		confirmDirection();
	}
	/**
	 * 判断我方飞机出界问题
	 * 
	 * */
	public boolean outOfBounds() {
		if(x<0) x=0;
		if(x>Constant.GAME_WIDTH-this.img.getWidth(null)) x=Constant.GAME_WIDTH-this.img.getWidth(null);
		if(y<30) y=30;
		if(y>Constant.GAME_HEIGHT-this.img.getHeight(null)) y=Constant.GAME_HEIGHT-this.img.getHeight(null);
		return true;
	}

	@Override
	public void fire() {
		switch(level) {
			case 1:
				Bullet bullet1=new Bullet(this.x+this.img.getWidth(null)/2-5, this.y,Direction.UP,pws,good,0);
				//将发出的子弹添加到主类中的容器中
				pws.bullets.add(bullet1);
				break;
			case 2:
				Bullet bullet2=new Bullet(this.x+this.img.getWidth(null)/2-5, this.y,Direction.UP,pws,good,0);
				bullet2.img=Images.imgs.get("bullet_02");
				bullet2.setPower(20);
				pws.bullets.add(bullet2);
				break;
			case 3:
				for(int i=0;i<3;i++) {
					Bullet bullet3=new Bullet(this.x+i*30, this.y,Direction.UP,pws,good,0);
					//将发出的子弹添加到主类中的容器中
					pws.bullets.add(bullet3);
				}
				break;
			case 4:
				for(int i=0;i<3;i++) {
					Bullet bullet4=new Bullet(this.x+i*30, this.y,Direction.UP,pws,good,0);
					bullet4.img=Images.imgs.get("bullet_02");
					bullet4.setPower(20);
					//将发出的子弹添加到主类中的容器中
					pws.bullets.add(bullet4);
				}
				break;
			default:
				Bullet bullet5=new Bullet(this.x, this.y,Direction.LEFT_UP,pws,good,0);
				bullet5.img=Images.imgs.get("bullet_02");
				for(int i=0;i<3;i++) {
					Bullet bullet4=new Bullet(this.x+i*30, this.y,Direction.UP,pws,good,0);
					bullet4.img=Images.imgs.get("bullet_02");
					//将发出的子弹添加到主类中的容器中
					pws.bullets.add(bullet4);
					bullet4.setPower(20);
				}
				Bullet bullet6=new Bullet(this.x+this.img.getWidth(null)/2+60, this.y-30,Direction.RIGHT_UP,pws,good,0);
				bullet6.img=Images.imgs.get("bullet_02");
				bullet5.setPower(20);
				bullet6.setPower(20);
				pws.bullets.add(bullet5);
				pws.bullets.add(bullet6);
				break;
		}
	}
	
private BloodBar bb=new BloodBar();
	
	/**
	 * 画血条:血条内部类
	 * 内部类能够直接访问外部类的成员变量和方法
	 * 而外部类不能访问内部类的成员变量和方法
	 * */
	class BloodBar{
		public void draw(Graphics g) {
			Color c=g.getColor();
			if(blood>(100+(level-1)*25)*0.75) {
				g.setColor(Color.green);
			}else if(blood<=(100+(level-1)*25)*0.75&&blood>(100+(level-1)*25)*0.3) {
				g.setColor(Color.orange);
			}else {
				g.setColor(Color.RED);
			}
			g.drawRect(x, y-15, img.getWidth(null), 10);
			g.fillRect(x, y-15, img.getWidth(null)*blood/(100+(level-1)*25), 10);
			g.setColor(c);
		}
	}
	
	public boolean eatItem(Item i) {
		switch(i.getCategory()){
			case 0:
				if(this.good&&this.live&&i.isLive()&&this.getRect().intersects(i.getRect())) {
					if(this.blood<(75+this.getLevel()*25)) {
						this.setBlood(75+this.getLevel()*25);
					}else Bullet.hideScore+=500;
					new AePlayWave("src/com/planewar/sounds/eatItem.wav").start();
					i.setLive(false);
					return true;
				}
				break;
			case 1:
				if(this.good&&this.live&&i.isLive()&&this.getRect().intersects(i.getRect())) {
					if(this.getBlood()>=(75+this.getLevel()*25)) {
						Bullet.hideScore+=100;
					}else if(this.getBlood()<(75+this.getLevel()*25)&&this.getBlood()>(45+this.getLevel()*25)){
						this.setBlood(75+this.getLevel()*25);
					}else {
						this.setBlood(this.getBlood()+30);
					}
					new AePlayWave("src/com/planewar/sounds/eatItem.wav").start();
					i.setLive(false);
					return true;
				}
				break;
			case 2:
				if(this.good&&this.live&&i.isLive()&&this.getRect().intersects(i.getRect())) {
					if(this.getLevel()<5) {
						this.setLevel(this.getLevel()+1);
					}else {
						Bullet.score+=500;
						Bullet.hideScore+=500;
					}
					new AePlayWave("src/com/planewar/sounds/eatItem.wav").start();
						i.setLive(false);
						return true;
				}
				break;
			case 3:
				if(this.good&&this.live&&i.isLive()&&this.getRect().intersects(i.getRect())) {
					this.setLife(this.getLife()+1);
					new AePlayWave("src/com/planewar/sounds/eatAddLife.wav").start();
					i.setLive(false);
					return true;
				}
				break;
			case 4:
				if(this.good&&this.live&&i.isLive()&&this.getRect().intersects(i.getRect())) {
					new AePlayWave("src/com/planewar/sounds/eatItem.wav").start();
					for(Plane ep:pws.enemyPlanes) {
						Explode e=new Explode(ep.x, ep.y, pws);
						pws.explodes.add(e);
						//生成一个道具
						if(r.nextInt(1000)>(980-Bullet.stage*2)) {
							Item item=new Item(pws,ep.x,ep.y,0);
							pws.items.add(item);
						}
						if(r.nextInt(1000)>(970-Bullet.stage*4)) {
							Item item=new Item(pws,ep.x,ep.y,1);
							pws.items.add(item);
						}
						if(r.nextInt(1000)>(980-Bullet.stage*3)) {
							Item item=new Item(pws,ep.x,ep.y,2);
							pws.items.add(item);
						}
						if(r.nextInt(1000)>(990-Bullet.stage*2)) {
							Item item=new Item(pws,ep.x,ep.y,3);
							pws.items.add(item);
						}
						if(r.nextInt(1000)>(995-Bullet.stage)) {
							Item item=new Item(pws,ep.x,ep.y,4);
							pws.items.add(item);
						}
						new AePlayWave("src/com/planewar/sounds/explodePlane.wav").start();
					}
					pws.enemyPlanes.clear();
					pws.bullets.clear();
					i.setLive(false);
					return true;
				}
				break;
		}
		
		return false;
	}
	
	public boolean eatItem(List<Item> items) {
		for(int i=0;i<items.size();i++) {
			Item item=items.get(i);
			if(this.eatItem(item)) return true;
		}
		return false;
	}
	
	public boolean hitPlane(Plane plane) {
		if(plane.isLive()&&this.isLive()&&this.getRect().intersects(plane.getRect())) {
			if(plane instanceof Boss) {
				this.setBlood(0);
				Explode e=new Explode(this.x, this.y,pws);
				this.pws.explodes.add(e);
				new AePlayWave("src/com/planewar/sounds/explodeSelf.wav").start();
				//飞机消失
				this.setLive(false);
			}else {
				this.setBlood(this.getBlood()-plane.getLevel());
				new AePlayWave("src/com/planewar/sounds/hitPlane.wav").start();
				if(this.getBlood()<=0){
					Explode e=new Explode(this.x, this.y,pws);
					this.pws.explodes.add(e);
					new AePlayWave("src/com/planewar/sounds/explodeSelf.wav").start();
					//飞机消失
					this.setLive(false);
				}
			}
			return true;
		}
		return false;
	}
	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public boolean hitPlane(List<Plane> planes) {
		for(int i=0;i<planes.size();i++) {
			Plane plane=planes.get(i);
			if(this.hitPlane(plane)) return true;
		}
		return false;
	}
}
