package com.planewar.entity;

import java.awt.Graphics;
import java.awt.Rectangle;

import com.planewar.client.PlaneWarSystem;
import com.planewar.core.PlaneWarObject;

/**
 * 
 * �ϰ�����
 * @author ����
 * @since 2017-009-04
 * @version 1.0
 * */
public class Obstruction extends PlaneWarObject{
	PlaneWarSystem pws;
	private int width;
	private int height;
	private int speed;
	private boolean live;
	
	public Obstruction() {
		// TODO Auto-generated constructor stub
	}
	
	public Obstruction(int x,int y,int width,int height,PlaneWarSystem pws) {
		this.x=x;
		this.y=y;
		this.width=width;
		this.height=height;
		this.pws=pws;
		this.speed=3;
		this.live=true;
	}

	@Override
	public void draw(Graphics g) {
		g.fillRect(x, y, width, height);
	move();
	}

	@Override
	public void move() {
		
	}
	@Override
	public Rectangle getRect() {
		return new Rectangle(x, y, width, height);
	}
}
