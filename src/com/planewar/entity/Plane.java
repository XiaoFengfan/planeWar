package com.planewar.entity;

import java.awt.Graphics;
import java.util.Random;
import com.planewar.client.PlaneWarSystem;
import com.planewar.core.Direction;
import com.planewar.core.PlaneWarObject;

/**
 * 
 * 飞机主类
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 * */
public abstract class Plane extends PlaneWarObject{
	protected int speed;
	/**
	 * 表示飞机运动方向的boolean类型
	 * 
	 * */
	protected boolean left,up,right,down;
	/**
	 * 飞机真正的方向，枚举类型
	 * */
	public Direction dir;
	
	/**
	 * 区分敌我飞机的好坏
	 * */
	
	protected boolean good;
	
	/**
	 * 飞机的等级
	 * */
	protected int level;
	
	public boolean isGood() {
		return good;
	}

	public void setGood(boolean good) {
		this.good = good;
	}
	PlaneWarSystem pws;
	
	protected boolean live;

	public boolean isLive() {
		return live;
	}

	public void setLive(boolean live) {
		this.live = live;
	}
	

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Plane() {}
	/**
	 * 飞机的构造方法
	 * */
	public Plane(int x,int y,PlaneWarSystem pws) {
		this.x=x;
		this.y=y;
		this.pws=pws;
		this.live=true;
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(img, x, y, null);
	}

	@Override
	public void move() {
		//确定方向后如何移动
		switch (dir) {
		case LEFT:
			x-=speed;
			break;
		case LEFT_UP:
			x-=speed;
			y-=speed;
			break;
		case UP:
			y-=speed;
			break;
		case RIGHT_UP:
			x+=speed;
			y-=speed;
			break;
		case RIGHT:
			x+=speed;
			break;
		case RIGHT_DOWN:
			x+=speed;
			y+=speed;
			break;
		case DOWN:
			y+=speed;
			break;
		case LEFT_DOWN:
			x-=speed;
			y+=speed;
			break;
		default:
			dir=Direction.STOP;
			break;
		}
	}
	
	static Random r=new Random();
	
	
	public abstract void fire();
	
	public abstract boolean outOfBounds();
	
	public int getBlood() {
		return blood;
	}

	public void setBlood(int blood) {
		this.blood = blood;
	}
	/**
	 * 飞机的血量
	 * */
	protected int blood;
}
