package com.planewar.entity;

import java.awt.Graphics;
import com.planewar.client.PlaneWarSystem;
import com.planewar.constant.Constant;
import com.planewar.core.Images;
import com.planewar.core.PlaneWarObject;

/**
 * 
 * 飞机大战的道具类
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 * */
public class Item extends PlaneWarObject{
	PlaneWarSystem pws;
	private int speed;
	private double degree;
	private boolean live;
	private int category;//0:满血 1：小血 2：攻击力...
	
	public Item() {
		
	}
	
	public Item(PlaneWarSystem pws,int x,int y) {
		this.pws=pws;
		this.x=x;
		this.y=y;
		this.speed=10;
		this.live=true;
		this.degree=Math.random()*Math.PI*(1-1.0/30)+Math.PI/60;
		
	}
	public Item(PlaneWarSystem pws,int x,int y,int category) {
		this(pws, x, y);
		this.setCategory(category);
		switch(category) {
			case 0:this.img=Images.imgs.get("bloodItem_01");break;
			case 1:this.img=Images.imgs.get("bloodItem_02");break;
			case 2:this.img=Images.imgs.get("level_up");break;
			case 3:this.img=Images.imgs.get("addLife");break;
			case 4:this.img=Images.imgs.get("boom");break;
		}
	}

	@Override
	public void draw(Graphics g) {
		if(!live||this.y>Constant.GAME_HEIGHT||y<0) {
			pws.items.remove(this);
			return;
		}
		g.drawImage(img, x, y, null);
		move();
	}

	@Override
	public void move() {
		x+=(int)(speed*Math.cos(degree));
		y+=(int)(speed*Math.sin(degree));
		
		if(x<3||x>Constant.GAME_WIDTH-this.img.getWidth(null)) degree=Math.PI-degree;
		
	}

	public boolean isLive() {
		return live;
	}

	public void setLive(boolean live) {
		this.live = live;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}
	
}
