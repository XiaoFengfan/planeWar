package com.planewar.entity;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.planewar.client.PlaneWarSystem;
import com.planewar.constant.Constant;
import com.planewar.core.Direction;
import com.planewar.core.Images;

public class Boss extends Plane{
	public static Image[] imgs= {
			Images.imgs.get("boss_01"),
			Images.imgs.get("boss_02"),
			Images.imgs.get("boss_03"),
			Images.imgs.get("boss_04"),
			Images.imgs.get("boss_05"),
			Images.imgs.get("boss_06")
	};
	
	private boolean right;
	
	public Boss() {
		
	}
	
	public Boss(PlaneWarSystem pws) {
		this.setLive(true);
		this.pws=pws;
		this.img=imgs[0];
		this.x=Constant.GAME_WIDTH/2-img.getWidth(null)/2;
		this.y=-120;
		if(Bullet.stage<3) {
			this.speed=5+3*Bullet.stage;
		}else if(Bullet.stage==5){
			this.speed=8;
		}else {
			this.speed=5+2*(Bullet.stage-3);
		}
		this.blood=3000;
		this.dir=Direction.STOP;
		this.right=true;
		this.good=false;
	}
	
	@Override
	public void move() {
		super.move();
		if(pws.bg.getSpeed()>10) {
			this.dir=Direction.DOWN;
		}
		if(Bullet.stage<=3) {
			if(y>100) {
				y=100;
				if(x>Constant.GAME_WIDTH-this.img.getWidth(null)) right=false;
				if(x<0) right=true;
				if(right) this.dir=Direction.RIGHT;
				else this.dir=Direction.LEFT;
			}
		}
		if(Bullet.stage>3) {
			if(y>100&&y<115&&x>50) {
				y=100;
				if(x>Constant.GAME_WIDTH-this.img.getWidth(null)) right=false;
				if(x<0) right=true;
				if(right) this.dir=Direction.RIGHT;
				else this.dir=Direction.LEFT;
			}
			if(x<=50&&y<600) {
				x=50;
				this.dir=Direction.DOWN;
			}
			if(y>=600&&x<400) {
				y=600;
				this.dir=Direction.RIGHT;
			}
			if(x>=300&&y>100) {
				x=300;
				this.dir=Direction.UP;
			}
		}
		
		if(r.nextInt(100)>(95-Bullet.stage)&&y<400&&Bullet.stage<6) {
			fire();
		}
		if(r.nextInt(100)>(95-Bullet.stage)&&Bullet.stage==6) {
			fire();
		}
	}
	
	@Override
	public void draw(Graphics g) {
		super.draw(g);
		if(!this.isLive()) pws.bosses.remove(this);
		if(this.pws.bosses.size()>0&&this.pws.bosses.get(0).y>0) bb.draw(g);
		move();
	}
	
private BloodBar bb=new BloodBar();
	
	/**
	 * 画血条:血条内部类
	 * 内部类能够直接访问外部类的成员变量和方法
	 * 而外部类不能访问内部类的成员变量和方法
	 * */
	class BloodBar{
		public void draw(Graphics g) {
			Color c=g.getColor();
			if(blood>2000) {
				g.setColor(Color.green);
			}else if(blood<=2000&&blood>1000) {
				g.setColor(Color.orange);
			}else {
				g.setColor(Color.RED);
			}
			g.drawRect(80, 40,400 , 20);
			g.fillRect(80, 40, 400*blood/(2000+Bullet.stage*1000), 20);
			g.setColor(c);
		}
	}

	@Override
	public void fire() {
		//枚举类型转化为数组
		Direction[] dirs=Direction.values();
		for (int i = 0; i < dirs.length-1; i++) {
			Bullet b=new Bullet(x+img.getWidth(null)/2, y+img.getHeight(null)/2, dirs[i], pws, good,Bullet.stage-1);
			if(Bullet.stage==6) b.setSpeed(14);
			pws.bullets.add(b);
		}
		
	}

	@Override
	public boolean outOfBounds() {
		// TODO Auto-generated method stub
		return false;
	}
	
}
