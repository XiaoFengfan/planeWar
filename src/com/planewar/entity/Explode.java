package com.planewar.entity;

import java.awt.Graphics;
import java.awt.Image;

import com.planewar.client.PlaneWarControler;
import com.planewar.client.PlaneWarSystem;
import com.planewar.core.AePlayWave;
import com.planewar.core.Images;
import com.planewar.core.MyFrame;
import com.planewar.core.PlaneWarObject;

/**
 * 
 * 爆炸类
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 * */
public class Explode extends PlaneWarObject{
	
	static Image[] imgs= {
			Images.imgs.get("e1"),
			Images.imgs.get("e2"),
			Images.imgs.get("e3"),
			Images.imgs.get("e4"),
			Images.imgs.get("e5"),
			Images.imgs.get("e6"),
			Images.imgs.get("e7"),
			Images.imgs.get("e8"),
			Images.imgs.get("e9"),
			Images.imgs.get("e10"),
			Images.imgs.get("e11"),
			Images.imgs.get("e12"),
			Images.imgs.get("e13"),
			Images.imgs.get("e14"),
			Images.imgs.get("e15"),
			Images.imgs.get("e16"),
			Images.imgs.get("e17"),
			Images.imgs.get("e18"),
			Images.imgs.get("e19"),
			Images.imgs.get("e20"),
			Images.imgs.get("e21"),
			Images.imgs.get("e22"),
			Images.imgs.get("e23")
	};
	
	int step=0;
	
	private boolean live;
	
	PlaneWarSystem pws;
	
	public Explode() {
		// TODO Auto-generated constructor stub
	}
	
	public Explode(int x,int y,PlaneWarSystem pws) {
		this.x=x;
		this.y=y;
		this.setLive(true);
		this.pws=pws;
	}

	@Override
	public void draw(Graphics g) {
		/**for(int i=0;i<imgs.length;i++) {
			g.drawImage(imgs[i], x, y, null);
		}*/
		
		if(!live) {
			this.pws.explodes.remove(this);
			return;
		}
		if(step>imgs.length-1) {
			this.setLive(false);
			step=0;
			if(!pws.myPlane.isLive()&&pws.myPlane.getLife()>0) {
				new AePlayWave("src/com/planewar/sounds/reborn.wav").start();
			}
			return;
		}
		g.drawImage(imgs[step], x, y+10, null);
		step++;
	}
	
	public static boolean isHighestScore() {
		int ohs=0;//旧的最高分
		PlaneWarControler.getHighestScore();
		for(int i=0;i<PlaneWarControler.strh.length();i++) {
			ohs+=Math.pow(10, PlaneWarControler.strh.length()-i-1)*(PlaneWarControler.strh.charAt(i)-'0');
		}
		return Bullet.hideScore>ohs;
	}

	@Override
	public void move() {
		// TODO Auto-generated method stub
		
	}

	public boolean isLive() {
		return live;
	}

	public void setLive(boolean live) {
		this.live = live;
	}

}
