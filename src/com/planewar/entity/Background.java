package com.planewar.entity;

import java.awt.Graphics;
import java.awt.Image;

import com.planewar.constant.Constant;
import com.planewar.core.Images;
import com.planewar.core.PlaneWarObject;

/**
 * 
 * ����ͼ��
 * @author ����
 * @since 2017-009-04
 * @version 1.0
 * */
public class Background extends PlaneWarObject{
	private int speed;
	public int getSpeed() {
		return speed;
	}


	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public static Image[] imgs= {
			Images.imgs.get("stage_01"),
			Images.imgs.get("stage_02"),
			Images.imgs.get("stage_03"),
			Images.imgs.get("stage_04"),
			Images.imgs.get("stage_05"),
			Images.imgs.get("stage_06")
	};
	private int y1;
	private int y2;
	private int time=0;
	
//	public Background() {
//		// TODO Auto-generated constructor stub
//	}
	
	public Background() {
		this.img=imgs[0];
		this.x=0;
		this.y1=-img.getHeight(null);
		this.y2=0;
		this.y=Constant.GAME_HEIGHT-img.getHeight(null);
		this.speed=3;
	}


	@Override
	public void draw(Graphics g) {
		move();
		g.drawImage(img, x, y1, null);
		g.drawImage(img, x, y2, null);
		
	}

	@Override
	public void move() {
		if(y2>Constant.GAME_HEIGHT) {
			y2=y1-img.getHeight(null);
		}
		if(y1>Constant.GAME_HEIGHT) {
			y1=y2-img.getHeight(null);
		}
		y1+=speed;
		y2+=speed;
		if(speed<12) {
			time++;
			if(time>600) {
				time=0;
				speed++;
			}
		}
		
	}

}
