package com.planewar.entity;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Random;

import com.planewar.client.PlaneWarSystem;
import com.planewar.constant.Constant;
import com.planewar.core.Direction;
import com.planewar.core.Images;

/**
 * 
 * 敌方飞机类
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 * */
public class EnemyPlane extends Plane{
	
	private Random r=new Random();
	
	private static Image[] imgs= {
			Images.imgs.get("enemyPlane_01"),
			Images.imgs.get("enemyPlane_02"),
			Images.imgs.get("enemyPlane_03"),
			Images.imgs.get("enemyPlane_04"),
			Images.imgs.get("enemyPlane_05"),
			Images.imgs.get("enemyPlane_06")
	};
	
	public EnemyPlane() {
		
	}
	
	public EnemyPlane(int x,int y,PlaneWarSystem pws,int level) {
		super(x,y,pws);
		this.level=level;
		this.dir=Direction.DOWN;
		this.speed=level*3;
		this.good=false;
		this.blood=10+30*level;
		switch(level) {
		case 1:this.img=imgs[0];break;
		case 2:this.img=imgs[1];break;
		case 3:this.img=imgs[2];break;
		case 4:this.img=imgs[3];break;
		case 5:this.img=imgs[4];break;
		case 6:this.img=imgs[5];break;
		}
	}
	
	@Override
	public void draw(Graphics g) {
		if(!live||outOfBounds()) {
			this.pws.enemyPlanes.remove(this);
			return;
		}
		super.draw(g);
		move();
	}
	
	@Override
	public void move() {
		super.move();
		if(r.nextInt(100)>98-this.level) {
			fire();
		}

	}

	@Override
	public void fire() {
		Bullet bullet=new Bullet(this.x+this.img.getWidth(null)/2-5, this.y+20,Direction.DOWN,pws,good,this.level-1);
		//将发出的子弹添加到主类中的容器中
		pws.bullets.add(bullet);
		
	}

	@Override
	public boolean outOfBounds() {
		return x<0||x>Constant.GAME_WIDTH||y>Constant.GAME_HEIGHT;
	}

}
