package com.planewar.core;
/**
 * 
 * 所有类的父类
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 * */
import java.awt.Image;
import java.awt.Rectangle;

public abstract class PlaneWarObject implements Drawable,Moveable{
	public int x;//物体x坐标
	public int y;//物体y坐标
	public Image img;//物体所在的图片对象
	
	/**
	 * 获取当前图片对象所在的矩形
	 * @return 矩形
	 * */
	public Rectangle getRect() {
		return new Rectangle(x, y, img.getWidth(null), img.getHeight(null));
	}
}
