package com.planewar.core;
/**
 * 
 * 定义飞机运动方向的枚举类型
 * 枚举类型：直接定义常量的名称，不需要对常量进行权限修饰
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 * */
public enum Direction {
	LEFT,LEFT_UP,UP,RIGHT_UP,RIGHT,RIGHT_DOWN,DOWN,LEFT_DOWN,STOP
}
