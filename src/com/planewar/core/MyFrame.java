package com.planewar.core;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.planewar.client.PlaneWarControler;
import com.planewar.constant.Constant;
/**
 * 
 * @author 丁奇
 * 
 * @version 1.0
 * 
 * @since 2017-09-04
 * 
 * */
public class MyFrame extends Frame{
	
	//控制游戏结束
	public static boolean start=true;
	
	//控制暂停
	public static boolean isContinue=true;
	
	Image backImg=null;
	/**
	 * 加载窗口的方法
	 * */
	public void launchFrame() {
		this.setSize(Constant.GAME_WIDTH,Constant.GAME_HEIGHT);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setResizable(false);
		this.setTitle(Constant.GAME_TITTLE);
		new MyThread().start();
	}
	/**
	 * 线程内部类，用于启用画图片的线程
	 * 
	 * */
	
	class MyThread extends Thread {
		public void run() {
			while(start) {
				if(isContinue) {
					repaint();
				}
				try {
					Thread.sleep(40);
				}catch(InterruptedException e) {}
			}
		}
	}
	/**
	 * 重写update()方法，在窗口的里层添加一个虚拟的图片
	 * 避免图片出现闪烁情况
	 * 
	 * */
	public void update(Graphics g) {
		if(backImg==null) backImg=createImage(Constant.GAME_WIDTH,Constant.GAME_HEIGHT);
		Graphics backg=backImg.getGraphics();
		Color c=backg.getColor();
		backg.setColor(Color.WHITE);
		backg.fillRect(0, 0, Constant.GAME_WIDTH, Constant.GAME_HEIGHT);
		backg.setColor(c);
		paint(backg);
		g.drawImage(backImg, 0, 0, null);
	}
}
