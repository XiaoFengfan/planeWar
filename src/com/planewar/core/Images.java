package com.planewar.core;
/**
 * 
 * ��̬��ʼ����Ŀ�е�����ͼƬ
 * @author ����
 * @since 2017-009-04
 * @version 1.0
 * */

import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import com.planewar.constant.Constant;
import com.planewar.util.GameUtil;

public class Images {
	public static Map<String, Image> imgs=new HashMap<>();
	static {
		imgs.put("mainPlane_01", GameUtil.getImage(Constant.IMG_PRE+"mainPlane_01"+Constant.IMG_AFTER));
		
		//���˷ɻ�ͼƬ
		imgs.put("enemyPlane_01", GameUtil.getImage(Constant.IMG_PRE+"enemyPlane_01"+Constant.IMG_AFTER));
		imgs.put("enemyPlane_02", GameUtil.getImage(Constant.IMG_PRE+"enemyPlane_02"+Constant.IMG_AFTER));
		imgs.put("enemyPlane_03", GameUtil.getImage(Constant.IMG_PRE+"enemyPlane_03"+Constant.IMG_AFTER));
		imgs.put("enemyPlane_04", GameUtil.getImage(Constant.IMG_PRE+"enemyPlane_04"+Constant.IMG_AFTER));
		imgs.put("enemyPlane_05", GameUtil.getImage(Constant.IMG_PRE+"enemyPlane_05"+Constant.IMG_AFTER));
		imgs.put("enemyPlane_06", GameUtil.getImage(Constant.IMG_PRE+"enemyPlane_06"+Constant.IMG_AFTER));
		
		//�ӵ�ͼƬ
		imgs.put("bullet_01_up", GameUtil.getImage(Constant.IMG_PRE+"bullet_01_up"+Constant.IMG_AFTER));
		imgs.put("bullet_02", GameUtil.getImage(Constant.IMG_PRE+"bullet_02"+Constant.IMG_AFTER));
		imgs.put("enemyBullet_02", GameUtil.getImage(Constant.IMG_PRE+"enemyBullet_02"+Constant.IMG_AFTER));
		imgs.put("bullet_03", GameUtil.getImage(Constant.IMG_PRE+"bullet_03"+Constant.IMG_AFTER));
		imgs.put("bullet_04", GameUtil.getImage(Constant.IMG_PRE+"bullet_04"+Constant.IMG_AFTER));
		imgs.put("bullet_05", GameUtil.getImage(Constant.IMG_PRE+"bullet_05"+Constant.IMG_AFTER));
		imgs.put("bullet_06", GameUtil.getImage(Constant.IMG_PRE+"bullet_06"+Constant.IMG_AFTER));
		
		//��ըͼƬ
		imgs.put("e1", GameUtil.getImage(Constant.IMG_PRE+"e1"+Constant.IMG_AFTER));
		imgs.put("e2", GameUtil.getImage(Constant.IMG_PRE+"e2"+Constant.IMG_AFTER));
		imgs.put("e3", GameUtil.getImage(Constant.IMG_PRE+"e3"+Constant.IMG_AFTER));
		imgs.put("e4", GameUtil.getImage(Constant.IMG_PRE+"e4"+Constant.IMG_AFTER));
		imgs.put("e5", GameUtil.getImage(Constant.IMG_PRE+"e5"+Constant.IMG_AFTER));
		imgs.put("e6", GameUtil.getImage(Constant.IMG_PRE+"e6"+Constant.IMG_AFTER));
		imgs.put("e7", GameUtil.getImage(Constant.IMG_PRE+"e7"+Constant.IMG_AFTER));
		imgs.put("e8", GameUtil.getImage(Constant.IMG_PRE+"e8"+Constant.IMG_AFTER));
		imgs.put("e9", GameUtil.getImage(Constant.IMG_PRE+"e9"+Constant.IMG_AFTER));
		imgs.put("e10", GameUtil.getImage(Constant.IMG_PRE+"e10"+Constant.IMG_AFTER));
		imgs.put("e11", GameUtil.getImage(Constant.IMG_PRE+"e11"+Constant.IMG_AFTER));
		imgs.put("e12", GameUtil.getImage(Constant.IMG_PRE+"e12"+Constant.IMG_AFTER));
		imgs.put("e13", GameUtil.getImage(Constant.IMG_PRE+"e13"+Constant.IMG_AFTER));
		imgs.put("e14", GameUtil.getImage(Constant.IMG_PRE+"e14"+Constant.IMG_AFTER));
		imgs.put("e15", GameUtil.getImage(Constant.IMG_PRE+"e15"+Constant.IMG_AFTER));
		imgs.put("e16", GameUtil.getImage(Constant.IMG_PRE+"e16"+Constant.IMG_AFTER));
		imgs.put("e17", GameUtil.getImage(Constant.IMG_PRE+"e17"+Constant.IMG_AFTER));
		imgs.put("e18", GameUtil.getImage(Constant.IMG_PRE+"e18"+Constant.IMG_AFTER));
		imgs.put("e19", GameUtil.getImage(Constant.IMG_PRE+"e19"+Constant.IMG_AFTER));
		imgs.put("e20", GameUtil.getImage(Constant.IMG_PRE+"e20"+Constant.IMG_AFTER));
		imgs.put("e21", GameUtil.getImage(Constant.IMG_PRE+"e21"+Constant.IMG_AFTER));
		imgs.put("e22", GameUtil.getImage(Constant.IMG_PRE+"e22"+Constant.IMG_AFTER));
		imgs.put("e23", GameUtil.getImage(Constant.IMG_PRE+"e23"+Constant.IMG_AFTER));

		//����ͼƬ
		imgs.put("bloodItem_01", GameUtil.getImage(Constant.IMG_PRE+"bloodItem_01"+Constant.IMG_AFTER));
		imgs.put("bloodItem_02", GameUtil.getImage(Constant.IMG_PRE+"bloodItem_02"+Constant.IMG_AFTER));
		imgs.put("level_up", GameUtil.getImage(Constant.IMG_PRE+"level_up"+Constant.IMG_AFTER));
		imgs.put("addLife", GameUtil.getImage(Constant.IMG_PRE+"addLife"+Constant.IMG_AFTER));
		imgs.put("boom", GameUtil.getImage(Constant.IMG_PRE+"boom"+Constant.IMG_AFTER));
		
		//boss
		imgs.put("boss_01", GameUtil.getImage(Constant.IMG_PRE+"boss_01"+Constant.IMG_AFTER));
		imgs.put("boss_02", GameUtil.getImage(Constant.IMG_PRE+"boss_02"+Constant.IMG_AFTER));
		imgs.put("boss_03", GameUtil.getImage(Constant.IMG_PRE+"boss_03"+Constant.IMG_AFTER));
		imgs.put("boss_04", GameUtil.getImage(Constant.IMG_PRE+"boss_04"+Constant.IMG_AFTER));
		imgs.put("boss_05", GameUtil.getImage(Constant.IMG_PRE+"boss_05"+Constant.IMG_AFTER));
		imgs.put("boss_06", GameUtil.getImage(Constant.IMG_PRE+"boss_06"+Constant.IMG_AFTER));
		
		//����ͼƬ
		imgs.put("stage_01", GameUtil.getImage(Constant.IMG_PRE+"stage_01"+Constant.IMG_AFTER));
		imgs.put("stage_02", GameUtil.getImage(Constant.IMG_PRE+"stage_02"+Constant.IMG_AFTER));
		imgs.put("stage_03", GameUtil.getImage(Constant.IMG_PRE+"stage_03"+Constant.IMG_AFTER));
		imgs.put("stage_04", GameUtil.getImage(Constant.IMG_PRE+"stage_04"+Constant.IMG_AFTER));
		imgs.put("stage_05", GameUtil.getImage(Constant.IMG_PRE+"stage_05"+Constant.IMG_AFTER));
		imgs.put("stage_06", GameUtil.getImage(Constant.IMG_PRE+"stage_06"+Constant.IMG_AFTER));
	}
}
