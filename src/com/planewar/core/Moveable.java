package com.planewar.core;
/**
 * 
 * 飞机大战中所有实体类都要移动
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 * */
public interface Moveable {
	void move();
}
