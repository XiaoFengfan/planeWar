package com.planewar.core;

import java.awt.Graphics;

/**
 * 
 * 飞机大战中所有实体类都要画出来
 * @author 丁奇
 * @since 2017-009-04
 * @version 1.0
 * */
public interface Drawable {
	void draw(Graphics g);
}
